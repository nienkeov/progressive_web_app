const online = window.navigator.onLine;

window.addEventListener('load', (e) => {
    if (!window.indexedDB) {
        console.log("Your browser does not support IndexedDB");
    }

    if('serviceWorker' in navigator) {
        try {
          navigator.serviceWorker.register('sw.js');
          console.log(`SW registered`)
        } catch (error) {
          console.log(`SW registration failed`);
        }
      }
      
    loadShowCases();
    loadTags();
});
const main = document.querySelector('main');
const imageUrl = 'https://cmgt.hr.nl:8000';


async function updateShowCase() {
    const response = await fetch(`https://cmgt.hr.nl:8000/api/projects/`)
    const json = await response.json();

    main.innerHTML = json.projects.map(createProject).join('\n');

    if('serviceWorker' in navigator) {
        try {
            //service workes file needs to be in the root of the application
            navigator.serviceWorker.register('sw.js');
            console.log('SW registered')
        } catch (error) {
            console.log('SW failed')
        }
    }
}

async function loadTags() {
    if(navigator.onLine) {
        fetch("https://cmgt.hr.nl:8000/api/projects/tags").then(
            res => {
                return res.json();
            }
        ).then(data => {
            let text = '';
            for (let i = 0; i < data.tags.length; i++) {
                text += `<div>
                          <span>${data.tags[i]}</span>
                         </div>`;
            }
            document.querySelector(".tags").innerHTML = text;
        })
    } else {
        document.querySelector(".tags").innerHTML = "App offline";
    }
}

async function loadShowCases() {
    const res = await fetch(`https://cmgt.hr.nl:8000/api/projects/`);
    const json = await res.json();
  
    json.projects.map(createProject);
    json.projects.forEach((project) => {
      localforage.setItem(project._id, project).catch((err) =>{
        console.log(err);
      });
    });
}

function createProject(project) {
    localforage
  .getItem(project._id)
  .then(() => {
    document.querySelector("main").insertAdjacentHTML(
      "afterbegin",
      `<div class="container">
        <div class="card-panel recipe white row">
          <div class="project">
            <div>
              <h2>${project.title}</h2>
              <img src="https://cmgt.hr.nl:8000/${project.headerImage}">
              <p>${project.description}</p>
              <hr/>
              <p>${project.tags}</p>
            </div>
          </div>
        </div>
      </div>`
    );
  })
  .catch((err) => {
    console.log(err);
  });
}