importScripts('./localforage.js')

//cache static assets in the application 
const staticAssets = [
    './',
    './styles.css',
    './app.js',
    './localforage.js',
    'index.html'
];

//install event will get called when new service worker is discovered & gets installed
self.addEventListener('install', async (e) => {
    const cache = await caches.open("projects-static");
    cache.addAll(staticAssets);
});

self.addEventListener('activate', e => {
    let cacheWhiteList = ['projects-static']
  
    e.waitUntil(
      caches.keys().then (cacheNames => {
        return Promise.all(
          cacheNames.map (cacheName => {
            if(cacheWhiteList.indexOf(cacheName) === -1) {
              return caches.delete(cacheName)
            }
          })
        )
      })
    )
  })

  self.addEventListener('fetch', e => {
    const req = e.request;
    const url = new URL(req.url);
  
    if(url.origin === location.origin) {
      e.respondWith(cacheFirst(req));
    } else {
      e.respondWith(networkFirst(req));
    }
  });

async function cacheFirst(req) {
    const cachedResponse = await caches.match(req);
    // Check cache but fall back to network
    return cachedResponse || fetch(req);
}

async function networkFirst(req) {
    const dynamicCache = await caches.open('projects-dynamic');

    try {
      const res = await fetch(req)
      dynamicCache.put(req, res.clone())
      return res
    } catch (error) {
      const cachedRes = await dynamicCache.match(req)
      return cachedRes
    }
}